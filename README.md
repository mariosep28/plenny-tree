# Plenny Tree 🥤🌲

Plenny Tree is a project that contains a game for the Alva Majo's Jam. The theme was 'too large' so we have to do a game based in this theme.

These are the ideas that were thought for this game:

- A tree whose branches are constantly growing

- The user has different tools to cut these branches

- The branches have to be thown to a campfire that gives you time/points

- When a branch is **too large** will be hard to cut it and yo will lose time



